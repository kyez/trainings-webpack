document.addEventListener("DOMContentLoaded", function () {
  const _config = {
    dev: {
      address: "http://79.137.68.21",
      port: 8181
    },
    prod: {
      address: "http://localhost",
      port: 80
    }
  };
  const env = _config['dev'];

  let selected = [];

  const selectedElem = document.querySelector(".selected-trainings");
  const loaderElem = document.querySelector(".loader");
  const registrationBtnElem = document.querySelector(".registration-btn");
  const sumbmitBtnElem = document.querySelector(".submit-btn");

  const nameInputElem = document.querySelectorAll(".name-input");
  const emailInputElem = document.querySelectorAll(".email-input");
  const surnameInputElem = document.querySelector(".surname-input");
  const cityInputElem = document.querySelector(".city-input");
  const messageInputElem = document.querySelector(".message-input");

  const trainingEvent = new Event('trainigsReady');

  document.addEventListener('trainigsReady', () => {
    loaderElem.style.display = "none";
  });

  registrationBtnElem.addEventListener('click', e => {
    e.preventDefault();
    _sendPostData(e);
  });

  sumbmitBtnElem.addEventListener('click', e => {
    e.preventDefault();
    _sendPostData(e);
  });

  getTrainings().then(items => {
    // to remove - only for demo purposes
    // items = [];

    //todo - pytanie o text w liscie przy widoku mobilnym

    if (items.length) {
      document.dispatchEvent(trainingEvent);
      items.forEach(training => {
        //TODO - remove
        // training.imgUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAMAAAC/MqoPAAAAz1BMVEUAAADUBy/DDi7dAzDdAzDdAzDdAzDDDi7DDi7DDi7dAzDdAzDdAzDDDi7DDi7DDi7dAzDdAzDdAzDDDi7DDi7DDi7dAzDdAzDDDi7DDi7dAzDdAzDDDi7DDi7dAzDDDi7fEz3HHTvugZjhh5f97/L78PLqYn7////aaHz74OX44eXmQmTSSmL3wMvww8vhI0rLLEjyobHppbHdAzDDDi7jMlfOO1XoUnHWWW/50Nj00tjscYvdd4nwkaTllqT0sL7stL7hRGPXBjDWBi/FDS4+JsiBAAAARXRSTlMAMDAwj9///9+PIHDPz3AgEGC/v2AQUK+vUJ/v75+AgP////////////////////////9AQP//////////////////r6+TKVt1AAAH7ElEQVR4AezUtaHDUBTA0I9mZtx/zHDMWOY+nQ3U6AsAAAAAAAAAAAAA8Em+f9Ts/v3713TDVK7esh3tRr9xPV+d7iCMtCf9KU5SJcKzXOvonaIU313VmjZK7zRtKXtsY/qI1OlZ9rN7Jb2rlza9IHS0JfoSV9D0wlxboa8oElljO5HeTU/C2E6kC5heN7Yz6QKm143tTLqA6QXrYzub/pxeKmFsV2buQllxZQ3DcJZ1jwuMS7AYGmx84Jy97/+exjNGWLv+zvst+O7gKfnrha6Kna4/ethhq9wUvdIf99G7EV8407xp1zpHevTuff8JrqN//3H/8PgPG0/njx5/2Hg6f/T4w8bTj/bo3ahKNWjdXpC76ty7B/9vMXz9Qbic+0cTOGz2JanRChw94LC55svyvPDNd5VH7+zrQQc2zPORJ/bi5ekhD5t94/zLJoAcOHrEYTNs+pU+M/CAowccNmBl/m1zD646evxhQ7f4Tl96cvzRW1WHjVs3/7HfswY6emv+v0Vy/Yo+oOnUP5rVT1F8SUVPeTnz8/bMaZZV8ipr+J1GDSeiD3/RRyJ61HTW+2bImWoTifxFY3pLQp/+Tp9J6G2eDuZMtflx0mMFffEnfamgd0g6nzNk1vD0R8qcUWZN86BdKXNGmTXr5jknzBlp1gC/4YQ5I82aqPkuZDkjzZprAL0lyxlp1rQB+mNY/iqv3WuY/gSgx6qc0WZNB6DflDWstGbvAPSVKGfEWbM+Ono32UdPezAdmCZn1FkTERPlDJ81PP0WKH+TX7K3oPw2Qm8pckadNW2Efi7IGXnWXEfosSBn5FnTQej3+ZzRZ80DhL7ic0afNWuEfsbnjD5rTiNkfM7osyZi9pzOGX3WvIDoLTpn9FnTJul8zvBZw9NjOmf0WdNh6XzOLJZs1vD0R6qcGU9UWfMUoq9EOfPO+feirFlD9HuinMmcL4CsYZ9e+Kb5sGtMus730nxnH4mioXYhyZmNc95vJVlzDaO3JA1bfqXPJTXbxuiPFTkzdV/pfqbImicYPVa8ML75Tn+reHvsYPSbgpwZuu90PxJkzR2MvhLkTL+iDwRZsz4a+qZG163ovXx3W4AOjc+ZhavofslnTcQNz5l8/Is+ybms4em36Jx5537R/Xs6a26D9BadM9nv9ILOmjZIfwbnTNL9nd5L4ax5CdJjOGcW7ne6X8JZ0wHp9+HHpvJP+hx+hHoA0ldszkzdn3Q/Y7NmDdLP2JzJ/qYXbNacRuDQnBnufrVghGZNRA7Nmf4ufUBlDU9vkY9N5S59Tj5CtVk6mDMLt0v3SyhreHoMPjaN6+gT8BGqw9K5nBm6OrofAVmD0YEHmP/VeLJ6epHv7v/804t9Kyxnkm49vZdiWbNG6Tewhl24erpfYjV7N0JH5Uxe7qPPcyprInYXzAtjle+79PqQH/BPL+a1oJzJ9tMLKGvaMP0xkzNDt5/uR0zWPIHpsZ3+ri7f6+n7Q/69nd6h6UjO5OVl9HkOZA1PXyE5s3CX0f0SyZo1TSdyJh9fTp/kQNbg9IjImaG7nO5HRNZE9Iicyf6LXgBZw9NvWXMG2wB9etE3zZCjj/RFQz7AZDm4wvj0Qi825gw4W9Z0cPp9W86gm9ieXuitbDmDzpQ1a5x+ZsoZeHP+6cUye85ws2RNdEh6N8fXOyi9pc8ZImvaB6UnPD09KD3W5wyRNR09nW9YpmYV9Ed8zlg24Z9e8KaZaugzumgMu6HPGSJr7kaC6XOGyJpIsQs+Z/isuSaht4Jzpj+u3z+TPRsEZ01bQn8cmjOJ27N/9wrS0Kx5IqHHoTmzsdO3oVnT0dMtOVPa6XN71ijpq8CcmTo73c8Cs2atpxtyJguhF/asEdKjsJxJXAjdp2FZE2kWljObMPrWnjVC+q2gnCnD6HN71tBPL4am6RuOXEU3HroBXzTIA0xiOHIV3XjoUvLpxbA4IGcSF0r3aUDWdET0+wE5swmnbwOy5oGIvgr42FAZTp8HfK5oLaKf2XNm6sLpfmbPmtNINPvHhrIm9ML+uaJINXPOJK4J3afmrJHRW8aGzTfN6NvcWLNtHd362FQ2o8+tj1A6emz8duLUNaP7mfErjJ0D0DPDkTPQC+MjlI7+yJYziWtK96kta57K6Ctbzmya07e2rFnL6Ddsj01lc/rc9gh1N5LNlDNT15zuZ6asiXS7sDw2ZQS9sDxCXRPSW4acSRxB96kha9pC+mNDzmwY+taQNU+E9NjwKeiSoc8NH5fuXDW97NctcwzdF4O6za+avvrcnl3Y6A5DQRS+PzMzF5FUMO/139KSeJmONdLe08EIvsR29+e9Of3n1TkdyXt6kI1OvtPP00CbX12n3zZBNzw6Tr/MokTV0m36qo5SbTtO0/uHYAO8k79ulHfy143yTv66Ud6J183VO/G6uXonWDfeu1P56WdWN9478brhtZYlp6+a4VTVKTW9X4dbi1OJ6ed1/DwD78Tr5uqdeN1cvROvm6t34nVz9U68bq7eidfN1Tvxurl6J0A3h6rxb0yfELrxLTo/nd5ndDPwTj66AeOP359+YYfzDZffm74CWTfwTrxurt6J183VO/G6uXonXjdX78Tr5uqdeN1cvROvm6t3ctYNGN9+ffoAGG7XcPdy+t5aN+BxWvxjsat3InTz79E7PekWQPbeyV83qOG//7PI/mhZlmVZlmVZlmVZlmXZPZmSvHpA7pEOAAAAAElFTkSuQmCC"
        training.imgUrl = `data:image/png;base64,${training.imgUrl}`;
        generateTrainingElement(training);
      });
    } else {
      loaderElem.innerText = "Brak dostępnych szkoleń";
    }
  });

  async function getTrainings() {
    const res = await fetch(`${env.address}:${env.port}/projects`);
    return await res.json();
  }

  function _sendPostData() {
    const requestData = {
      first_name: nameInputElem[0].value !== "" ? nameInputElem[0].value : nameInputElem[1].value,
      last_name: surnameInputElem.value,
      email: emailInputElem[0].value !== "" ? emailInputElem[0].value : emailInputElem[1].value,
      location: cityInputElem.value,
      additionalMessage: messageInputElem.value,
      courses: selected.map(el => el.dataset.title)
    };

    if (validateEmail(requestData.email)) {
      fetch(`${env.address}:${env.port}/send`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(requestData)
      }).then(() => {
        alert("Twoja wiadomość została wysłana!")
      })
    }
  }

  function validateEmail(email) {
    return email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  }


  document.querySelector('.trainings-list').addEventListener('click', (e) => {
    if (e.target.checked && e.target.type === 'checkbox') {
      selected.push(e.target);
    } else {
      if (selected.includes(e.target)) {
        selected.splice(selected.indexOf(e.target), 1);
      }
    }
    generateSelectedItems();
  });

  document.querySelector(".selected-trainings").addEventListener('click', e => {
    selected.forEach(el => {
      if (el.dataset.title == e.target.parentElement.innerText.slice(0, -1).trim()) {
        selected.splice(selected.indexOf(el), 1);
        el.checked = false;
        generateSelectedItems();
      }
    });
  });

  function createNewSelectedElement(name) {
    const newSelectedTrainingElem = document.createElement("span")
    const removeSelectionElem = document.createElement("span");

    newSelectedTrainingElem.classList.add('selected-training');
    removeSelectionElem.classList.add("remove-selection");
    removeSelectionElem.innerText = " x";
    newSelectedTrainingElem.innerText = name;
    newSelectedTrainingElem.appendChild(removeSelectionElem);

    return newSelectedTrainingElem;
  }

  function generateSelectedItems() {
    selectedElem.innerHTML = "";

    if (selected.length) {
      selectedElem.parentElement.style.display = "block";
    } else {
      selectedElem.parentElement.style.display = "none";
    }

    selected.forEach((e) => {
      selectedElem.appendChild(createNewSelectedElement(e.dataset.title));
    })
  }

  function generateTrainingElement(training) {
    const wrapper = document.querySelector(".container.trainings-list.content-section");

    const containterElem = document.createElement("div");
    containterElem.classList.add("training-element-container");

    containterElem.appendChild(_generateIconElement(training));
    containterElem.appendChild(_generateContentElement(training));
    containterElem.appendChild(_generateSelectionElement(training));

    wrapper.appendChild(containterElem);
  }

  function _generateIconElement(training) {
    const iconElem = document.createElement("div");
    iconElem.classList.add("icon");

    const imgElem = document.createElement("img");
    imgElem.src = training.imgUrl;

    iconElem.appendChild(imgElem);

    return iconElem;
  }

  function _generateContentElement(training) {
    const contentElem = document.createElement("div");
    contentElem.classList.add("content");

    const authorElem = document.createElement("span");
    authorElem.classList.add("author");
    authorElem.innerText = training.author;

    const titleElem = document.createElement("h3");
    titleElem.classList.add("title");
    titleElem.innerText = training.title;

    const descElem = document.createElement("p");
    descElem.classList.add("description");
    descElem.innerText = training.shortDescription;

    const showMoreElem = document.createElement("a");
    showMoreElem.classList.add("show-more");

    contentElem.appendChild(authorElem);
    contentElem.appendChild(titleElem);
    contentElem.appendChild(descElem);
    contentElem.appendChild(showMoreElem);

    return contentElem;

  }

  function _generateSelectionElement(training) {
    const selectionElem = document.createElement("div");
    selectionElem.classList.add("selection");

    const titleElm = document.createElement("span");
    titleElm.innerText = "Jestem zainteresowany/a";

    const inputElem = document.createElement("input");
    inputElem.type = "checkbox";
    inputElem.dataset.title = training.title;

    selectionElem.appendChild(titleElm);
    selectionElem.appendChild(inputElem);

    return selectionElem;
  }
});
